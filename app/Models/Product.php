<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsToMany('App\Models\ProductCategory', 'category_product_pivot', 'product_id', 'category_product_id')
            ->withTimestamps();
    }
    
}
