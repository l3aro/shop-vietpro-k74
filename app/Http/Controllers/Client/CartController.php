<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    /**
     * Get current cart
     */
    public function index() {
        return view('client.cart');
    }

    /**
     * Add new product to cart
     */
    public function add($product_id) {
        return 'Add new product to cart';
    }

    /**
     * Update row quantity
     */
    public function update($row_id, $quantity) {
        return 'Update row quantity';
    }

    /**
     * Delete an entry
     */
    public function delete($row_id) {
        return 'Delete an entry';
    }
}
