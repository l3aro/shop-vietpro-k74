<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Product;
use App\Models\Order;

class ProductController extends Controller
{
    /**
     * Get all product
     */
    public function index(Request $request) {
        $data = [];

        $list = Product::all();
        

        $data['list'] = $list;
        $data['title'] = 'Danh sách sản phẩm';

        return view('admin.product.index', $data);

    }

    /**
     * Get product detail
     */
    public function show() {
        return view('admin.product.');
    }

    /**
     *  Add new/update product
     */
    public function edit() {
        return ' Add new/update product';
    }

    /**
     * Delete a product
     */
    public function destroy() {
        return 'Delete a product';
    }

    /**
     * Add a product
     */
    public function add() {
        session()->flash('status', 'warning');
        return redirect('admin/product');
    }
}
