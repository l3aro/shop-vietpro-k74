<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * List user
     */
    public function index() {
        return 'List user';
    }

    /**
     * Get user detail
     */
    public function show() {
        return 'Get user detail';
    }

    /**
     * Add new/update user
     */
    public function edit() {
        return 'Add new/update user';
    }

    /**
     * Delete an user
     */
    public function destroy() {
        return 'Delete an user';
    }
}
