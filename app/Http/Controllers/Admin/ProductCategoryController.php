<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    /**
     * Get all product categories
     */
    public function index() {
        return 'Get all product categories';
    }

    /**
     * Get product category detail
     */
    public function show() {
        return 'Get product category detail';
    }

    /**
     *  Add new/update product category
     */
    public function edit() {
        return ' Add new/update product category';
    }

    /**
     * Delete a product category
     */
    public function destroy() {
        return 'Delete a product category';
    }
}
